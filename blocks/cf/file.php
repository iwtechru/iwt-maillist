<?php

wp_nonce_field(plugin_basename(__FILE__), $this->nonce);
$html = '<input id="" type="file" name="' . $params['field_name'] . '" value="" size="25" />';
$html .= '<p class="description">';
if (!$params['field_value']) {
    $html .= __('You have no file attached to this post.', 'umb');
} else {
    $html .= ($params['field_value']);
} // end if
$html .= '</p><!-- /.description -->';
echo $html;
wp_nonce_field('_' . $params['name'], '_' . $params['name']);
