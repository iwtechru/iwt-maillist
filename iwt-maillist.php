<?php
/*
  Plugin Name: iwtech.ru Почтовая рассылка
  Plugin URI: http://iwtech.ru
  Description: Почтовая рассылка
  Author: Nikita Menshutin
  Version: 1.0
  Author URI: http://iwtech.ru
 */

require('iwt-pluglib.php');
require_once('simplexlsx.class.php');
require_once('html2pdf.class.php');
defined('ABSPATH') or die("No script kiddies please!");

class iwt_maillist extends iwt_fw {

    public $calendar;
    public $manual;

    public function __construct($name, $settings) {
        parent::__construct($name, $settings);
        $this->manual = __('Пожалуйста, загружайте файл csv.<br/> Вы можете в теле письма подставить значения из файла с помощью шорткода [[название колонки]]<br/>QR-код будет создаваться из колонки, если ее название будет заключено в тройные квадратные скобки.<br/> Например: [[[Колонка для QR-кода]]]<br/>Если нужно указать размер qr-кода, например ширину каждой стороны 300 точек, используйте следующий формат:'
                . '<br/>[[[Колонка для QR-кода|размер]]], например: [[[http://' . $_SERVER['HTTP_HOST'] . '|300]]]'
                . '<br/>Автозамена в письмах, отправляемых по адресам, указанном в поле для e-mail не выполняется');
        if ($this->version < 1.5) {
            if (!method_exists($this, 'notice')) {
                add_action('admin_notices', function() {
                    ?>
                    <div class="error">
                        <p><?php _e('Один из уставноленных плагинов от iwtech.ru устарел, пожалуйста, обновите до последней версии или обратитесь к разработчику', 'my-text-domain'); ?></p>
                    </div>
                    <?php
                });
            }
        }
        //$this->dbg("Запущен плагин iwt-workingdays");
        //$this->cron(time() + 10, array($this, 'check_base'), array('single' => true));
        //$this->delete_option('custom_fields');
        $labels = array(
            'name' => _x('Email Рассылка', 'post type general name', 'your-plugin-textdomain'),
            'singular_name' => _x('Email Рассылка', 'post type singular name', 'your-plugin-textdomain'),
            'menu_name' => _x('Email Рассылки', 'admin menu', 'your-plugin-textdomain'),
            'name_admin_bar' => _x('Email Рассылка', 'add new on admin bar', 'your-plugin-textdomain'),
            'add_new' => __('Добавить рассылку', 'maillist', 'your-plugin-textdomain'),
            'add_new_item' => __('Добавить рассылку', 'your-plugin-textdomain'),
            'new_item' => __('Новая рассылка', 'your-plugin-textdomain'),
            'edit_item' => __('Редактировать рассылку', 'your-plugin-textdomain'),
            'view_item' => __('Просмотр', 'your-plugin-textdomain'),
            'all_items' => __('Все рассылки', 'your-plugin-textdomain'),
            'search_items' => __('Искать рассылку', 'your-plugin-textdomain'),
            'parent_item_colon' => __('Шаблон рассылки:', 'your-plugin-textdomain'),
            'not_found' => __('Не найдено.', 'your-plugin-textdomain'),
            'not_found_in_trash' => __('Не найдено в корзине.', 'your-plugin-textdomain')
        );
        $this->register_cpt('maillist', array('labels' => $labels, 'show_in_menu' => true, 'public' => false, 'hierarchical' => true, 'supports' => array('title', /* 'custom-fields', */ 'page-attributes', 'editor', 'revisions')));
        $this->add_metabox('maillist', 'Информация', array('variant' => 'infobox'), $this->manual);
        $this->add_metabox('maillist', 'Введите контакты через запятую', array('variant' => 'textarea'));
        //$this->add_metabox('maillist', 'QR-код', array('variant' => 'textarea'));
        $this->add_metabox('maillist', 'Создавать PDF', array('variant' => 'true_or_false'));
        $this->add_metabox('maillist', 'Отправить', array('variant' => 'true_or_false'));
        $this->add_attachments('maillist', array('notsingle' => true, 'params' => array(), 'title' => 'Загрузите контакты'));
        add_action($this->prefix . '_send', array($this, 'send'));
        add_action('save_post', array($this, 'save_custom_fields'), 99);
        add_filter('manage_edit-maillist_columns', array($this, 'add_column'));
        add_action('manage_maillist_posts_custom_column', array($this, 'add_column_content'), 10, 2);
        add_filter('manage_edit-maillist_sortable_columns', array($this, 'add_column_sortable'));
    }

    public function check_base() {
        $this->dbg('Функция check_base');
        $this->calendar = parent::get_option('calendar');
    }

    public function save_custom_fields($pid) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        return;
        if ('trash' == get_post_status($pid)) {
            return;
        }
        if (get_post_type($pid) != 'maillist') {
            return;
        }
        $p = get_post($pid);
        if ($p->post_parent > 0) {
            $this->mail_process($pid);
            return;
        }
        if (!get_post_meta($pid, $this->simplify('Отправить') . 'maillist', true)) {
            return;
        }
        (update_post_meta($pid, 'otpravitmaillist', 0));

        $files = $this->get_attachments($pid, 'Загрузите контакты');        
        foreach ($files as $file_) {
            $file = get_attached_file($file_['id']);
            $mime = mime_content_type($file);
            if ($mime == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                $xls[] = new SimpleXLSX($file);
            } else {
                $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $csv_ = $this->csv_import($file);
                foreach ($csv_ as $v) {
                    $rows[] = array($v);
                }
            }
            foreach ($xls as $xls_) {
                $rows[] = $this->be_xlsx_assoc($xls_);
            }
        }
        $post_ = array(
            'post_status' => 'publish',
            'post_type' => 'maillist',
            'post_parent' => $pid,
            'post_content' => $p->post_content,
            'post_title' => get_the_title($pid) . ' '
        );
        foreach ($rows as $rows_) {
            foreach ($rows_ as $rows__) {
                $post__ = $post_;
                $post__['post_content'] = $this->qr_process($post__['post_content'], $rows__);
                foreach ($rows__ as $h => $val) {
                    $post__['post_content'] = str_replace('[[' . $h . ']]', $val, $post__['post_content']);
                    //$post__['post_content'] = str_replace('[[[' . $h . ']]]', $this->qr($val), $post__['post_content']);                    
                }
                //   $post__['post_content'] = preg_replace('/\[\[[^\]]*\]\]/', '', $post__['post_content']);
                $post__['post_title'] = $post__['post_title']; // . $rows__['e-mail'];
                $emem = str_replace('gaidel@mail.ru', 'nikita@iwtech.ru', $rows__['e-mail']);
                $create_posts[$emem] = $post__;
            }
        }
        $posts_to_check_ = get_posts(array(
            'posts_per_page' => -1,
            'post_parent' => $pid,
            'post_type' => 'maillist'
        ));
        foreach ($posts_to_check_ as $ptc) {
            $em = get_post_meta($ptc->ID, $this->simplify('Введите контакты через запятую') . 'maillist');
            $emem = str_replace('gaidel@mail.ru', 'nikita@iwtech.ru', $em[0]);
            $posts_to_check[$emem] = $ptc;
        }
        $emails_from_fields_ = (get_post_meta($pid, $this->simplify('Введите контакты через запятую') . 'maillist', true));

        $omit = array("\n", "\r", " ");
        foreach ($omit as $om) {
            $emails_from_fields_ = str_replace($om, '', $emails_from_fields_);
        }
        $emails_from_fields = explode(',', $emails_from_fields_);
        foreach ($emails_from_fields as $email) {

            $post__ = $post_;
            //$post__ = preg_replace('/\[\[[^\]]*\]\]/', '', $post_);
            $create_posts[$email] = $post__;
        }
        $counter = 0;
        foreach ($create_posts as $email => $cp) {
            if (!$posts_to_check[$email]) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    break;
                }
                $counter++;
                remove_action('save_post', array($this, 'save_custom_fields'), 99);
                $id = wp_insert_post($cp);
                update_post_meta($id, $this->simplify('Введите контакты через запятую') . 'maillist', $email);
                update_post_meta($id, $this->simplify('Отправить') . 'maillist', 1);
                $time = time() + 5 * $counter;
                $this->dbg('Запланирована отправка на ' . $time . ' ' . date('d/m/y H:i:s', $time));
                $this->dbg($this->prefix . '_send');
                //echo $email."!!!\n<br/>";
                update_post_meta($id, 'willbesent', $time);
                wp_schedule_single_event($time, $this->prefix . '_send', array($id));
            }
        }
        

    }

    public function add_column($columns) {
        //print_r($columns);
        //die();
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Title'),
            'emails' => __('Адресаты'),
            'sentstatus' => __('Статус отправки'),
            'date' => __('Date')
        );
        return $columns;
    }

    public function add_column_content($column, $post_id) {
        if(get_post_status($post_id)=='trash'){
            return;
        }
        define( 'MY_TIMEZONE', (get_option( 'timezone_string' ) ? get_option( 'timezone_string' ) : date_default_timezone_get() ) );

        switch ($column) {
            case 'emails':
                echo get_post_meta($post_id, $this->simplify('Введите контакты через запятую') . 'maillist', true);
                break;
            case 'sentstatus':
                $ready = get_post_meta($post_id, $this->simplify('Отправить') . 'maillist', true);
                $sentstatus = get_post_meta($post_id, 'sentstatus', true);
                $willbesent = get_post_meta($post_id, 'willbesent', true);
                if ($ready) {
                    if (strlen($willbesent)) {
                        if (time() <= $willbesent) {                            
                            echo 'Будет отправлено ' ;//.  date_i18n('d/m/Y H:i:s', $willbesent);
                            echo get_date_from_gmt( date( 'Y-m-d H:i:s', $willbesent ), 'd/m/Y H:i:s' );
                            echo "<br/> (через " . $this->seconds_to_time($willbesent) . ')';
                        } else {
                           // echo 'Должно было быть отправлено ' . date_i18n('d/m/Y H:i:s', $willbesent);
                            echo 'Должно было быть отправлено ';
                            echo get_date_from_gmt( date( 'Y-m-d H:i:s', $willbesent ), 'd/m/Y H:i:s' );
                            if ($willbesent > time() - (2 * 30 * 60)) {
                                //echo '<span style="color:red">Ошибка. Проверьте настройки отправки писем. Попытки отправить продолжаются.</span>';
                                echo '<br/><marquee><span style="color:blue;">Идёт отправка&nbsp;&nbsp;&nbsp;&nbsp;</span></marquee>';
                            }
                        }
                    } else {
                        echo 'Готово к отправке<br/>';
                    }
                }

                if (strlen($sentstatus)) {
                    echo '<span style="color:green;">Отправлено ' . get_date_from_gmt( date( 'Y-m-d H:i:s', $willbesent ), 'd/m/Y H:i:s' ) . '</span>';
                } else {
                    
                }
                break;
            default:
                break;
        }
    }

    public function qr_process($content, $fields) {
        echo $content;
        preg_match_all('#\[{3}([^\]]*)\]{3}#', $content, $matches);
        foreach ($matches[1] as $match) {
            $content = str_replace('[[[' . $match . ']]]', $this->qr($match, $fields), $content);
        }
        return $content;
    }

    public function qr($code, $fields) {
        $code_data = explode('|', $code);
        //print_r($code_data);        
        $size = '200x200';

        if (isset($code_data['1'])) {
            $size = $code_data['1'];
        }
        $code = 'https://chart.googleapis.com/chart?chs=' . $size . '&cht=qr&chl=' . urlencode($fields[$code_data[0]]) . '&chco=006591';
        return '<img src="' . $code . '" alt="qr" class="aligncenter "/>';
    }

    public function add_column_sortable($columns) {
        $columns['sentstatus'] = 'sentstatus';
        $columns['emails'] = 'emails';
        return $columns;
    }

    public function seconds_to_time($seconds) {
        $timeleft = $seconds - time();
        return gmdate("H:i:s", $timeleft);
    }

    public function send($id) {
        $this->dbg('Фунцкия отправки писем');
        if ('trash' == get_post_status($id)) {
            return;
        }
        $this->dbg('Запись №' . $id);
        if (!get_post_meta($id, $this->simplify('Отправить') . 'maillist', true)) {
            return;
        }
        $p = get_post($id);
        $this->dbg($p);
        $fields = get_post_custom($id);
        $this->dbg($fields);
        $this->dbg('Поле ' . $this->simplify('Введите контакты через запятую') . 'maillist');
        $mail_ = get_post_meta($id, $this->simplify('Введите контакты через запятую') . 'maillist', true);
        $this->dbg('Почта');
        $this->dbg($mail_);
        if ((is_string($mail_) && !strlen($mail_)) || (is_array($mail_ && empty($mail_)))) {
            $this->dbg('Почтовый адрес не обнаружен, возвращаем');
            return;
        }
        $omit = array("\n", "\r", " ");
        foreach ($omit as $om) {
            $mail_ = str_replace($om, '', $mail_);
        }
        $this->dbg('Убрали посторонние символы (пробелы, переносы), получили');
        $this->dbg($mail_);
        $emails = explode(',', $mail_);
        $this->dbg('Список адресов');
        $this->dbg($emails);
        $body = $p->post_content;
        $title = $p->post_title;
        foreach ($emails as $email) {
            $this->dbg("Отправляем письмо на адрес " . $email);
            $create_pdf = get_post_meta($id, $this->simplify('Создавать PDF') . 'maillist', true);
            if ($create_pdf) {
                $pdf = $this->pdf($id);
                if ($pdf) {
                    if (wp_mail($email, $title, $body, "Content-type: text/html", $pdf)) {
                        delete_post_meta($id, $this->simplify('Отправить') . 'maillist');
                        update_post_meta($id, ('sentstatus'), time());
                    }
                    return;
                }
            } else {
                if (wp_mail($email, $title, $body, "Content-type: text/html", $pdf)) {
                    delete_post_meta($id, $this->simplify('Отправить') . 'maillist');
                    update_post_meta($id, ('sentstatus'), time());
                }
                return;
            }
        }
        //  $this->dbg(wp_mail('nikita@iwtech.ru', $id, $id));
    }

    public function mail_process($pid) {
        if (!get_post_meta($pid, $this->simplify('Отправить') . 'maillist', true)) {
            return;
        }
        $fields = get_post_custom($pid);
        $mail = (get_post_meta($pid, $this->simplify('Введите контакты через запятую') . 'maillist', true));
        $p = get_post($pid);
        foreach ($email as $em) {
            
        }
        $this->dbg("Отправка");
        $this->send($pid);
    }

    public function pdf($id) {
        $post_ = get_post($id);
        $content = ($post_->post_content);
        $content = '<page backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm" style="font-family: freeserif"><br />' . nl2br($content) . '</page>';        
        $content=preg_replace('/\<\!--nopdf-->.*\<\!--\/nopdf-->/','',$content);        
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', $unicode = true, $encoding = 'UTF-8', array(0, 0, 0, 0));
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->WriteHTML($content);
        ($file = dirname(__FILE__) . '/pdf/letter_' . crc32($id) . '.pdf');
        $html2pdf->Output($file, 'F');
        if (file_exists($file)) {
            return $file;
        }
        return false;
    }

    public function be_xlsx_assoc($rows) {
        $rows_ = $rows->rows();
        $header = $rows_[0];
        unset($rows_[0]);
        foreach ($rows_ as $k => $row) {
            foreach ($header as $col => $h) {
                $h_ = preg_replace('/\ *\Z/', '', $h);
                $rows__[$k][$h_] = $row[$col];
            }
        }
        return $rows__;
    }

}

$wdays_fw = new iwt_maillist('Maillist', array('debug' => true));
if ($_SERVER['REQUEST_URI'] == '/wp-cron.php') {

    $posts_ = get_posts(array(
        'post_type' => 'maillist',
        'posts_per_page' => 1,
        'order' => 'rand',
        'meta_query' => array(
            array(
                'key' => $wdays_fw->simplify('Отправить') . 'maillist',
                'value' => 1),
            array(
                'key' => 'willbesent',
                'value' => time(),
                'compare' => '<=',
                'type' => 'numeric'
            )
    )));
#   print_r($posts_);
    $wdays_fw->send($posts_[0]->ID);
}
